#!/bin/bash

java \
  -D"fintechlabs.base_url=${BASE_URL}" \
  -D"spring.data.mongodb.uri=mongodb://${MONGODB_HOST}:27017/test_suite" \
  ${ENABLE_DEBUG:+-D"fintechlabs.devmode=true"} \
  ${ENABLE_START_REDIR:+-D"fintechlabs.startredir=true"} \
  ${JWKS:+-D"fintechlabs.jwks=${JWKS}"} \
  ${SIGNING_KEY:+-D"fintechlabs.signingKey=${SIGNING_KEY}"} \
  -D"oidc.google.clientid=${OIDC_GOOGLE_CLIENTID}" \
  -D"oidc.google.secret=${OIDC_GOOGLE_SECRET}" \
  -D"oidc.gitlab.clientid=${OIDC_GITLAB_CLIENTID}" \
  -D"oidc.gitlab.secret=${OIDC_GITLAB_SECRET}" \
  -D"oidc.admin.group=${OIDC_ADMIN_GROUP}" \
  -Djdk.tls.maxHandshakeMessageSize=65536 \
  $JAVA_EXTRA_ARGS \
  -jar /server/suite.jar

